﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TSPApprox
{
    public class Multigraph // moze byc grafem prostym
    {
        public double[,] distances;
        public int[,] multiplicities;
        private bool multigraph;
        public int[] degrees;

        public int VerticesCount { get { return distances.GetLength(0); } }
        
        public Multigraph(List<Point> points, bool multigraph = false) // tworzy graf z listy punktów
        {
            this.multigraph = multigraph;
            distances = new double[points.Count, points.Count];
            multiplicities = new int[points.Count, points.Count];
            degrees = new int[points.Count];

            for(int i=0; i< points.Count; i++)
            {
                degrees[i] = points.Count - 1; 
                for(int j=i+1; j<points.Count; j++)
                {
                    double distance = CalculateEuclideanDistance(points[i], points[j]);
                    distances[i, j] = distance;
                    distances[j, i] = distance;
                    multiplicities[i, j] = 1;
                    multiplicities[j, i] = 1;
                }
            }
        }      

        public Multigraph(int n, bool multigraph = false) // graf bez krawędzi o n wierzchołkach
        {
            this.multigraph = multigraph;
            distances = new double[n,n];
            multiplicities = new int[n,n];
            degrees = new int[n];
        }

        public Multigraph(Multigraph mg) // konstruktor kopiujący
        {
            multigraph = mg.multigraph;
            int n = mg.distances.GetLength(0);
            distances = new double[n, n];
            multiplicities = new int[n, n];
            degrees = new int[n];
            for (int i = 0; i < n; i++)
            {
                degrees[i] = mg.degrees[i];
                for (int j = i + 1; j < n; j++)
                {
                    distances[i, j] = mg.distances[i,j];
                    distances[j, i] = mg.distances[j,i];
                    multiplicities[i, j] = mg.multiplicities[i,j];
                    multiplicities[j, i] = mg.multiplicities[j,i];
                }
            }
        }

        public void SetEdge(int i, int j, double distance, int multiplicity) // ustawia dane dla krawędzi
        {
            if (i == j) return;
            distances[i, j] = distance;
            distances[j, i] = distance;

            // zmiana krotności
            int difference = multiplicity - multiplicities[i, j];

            multiplicities[i, j] = multiplicity;
            multiplicities[j, i] = multiplicity;

            degrees[i] += difference;
            degrees[j] += difference;
        }

        private double CalculateEuclideanDistance(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        private int CountEdges()
        {
            int edgesCount = 0;
            for(int i=0; i<degrees.Length;i++)
            {
                for(int j=i+1;j<degrees.Length;j++)
                {
                    edgesCount += multiplicities[i, j];
                }
            }
            return edgesCount;
        }

        private void RemoveEdge(int i, int j)
        {
            if (i == j) return;
            if (multiplicities[i, j] == 0) return;
            --multiplicities[i, j];
            --multiplicities[j,i];
            --degrees[i];
            --degrees[j];
        }

        private bool IsBridge(int i, int j, List<int> verticesToIgnore) // czy krawędź ij jest mostem
        {
            Multigraph copy = new Multigraph(this);
            copy.RemoveEdge(i, j); // jeśli usunięcie krawędzi rozspójni graf to krawędź jest mostem
            int n = copy.degrees.Length;
            bool[] visited = new bool[n];
            foreach (int v in verticesToIgnore) visited[v] = true; 
            Stack<int> s = new Stack<int>();
            s.Push(0);
            while(s.Count > 0)
            {
                int v = s.Pop();
                visited[v] = true;
                for(int k=0;k<n;k++)
                {
                    if (copy.multiplicities[v, k] == 0) continue;
                    if(visited[k] == false)
                    {
                        s.Push(k);
                    }
                }
            }

            for (int k = 0; k < n; k++)
            {
                if (visited[k] == false) return true;
            }
            return false;
        }

        public List<int> FindEulerCycle() 
        {
            List<int> eulerCycle = new List<int>();
            eulerCycle.Add(0);
            int v = 0; // obecnie rozpatrywany wierzchołek
            int edgesCount = CountEdges();
            Multigraph copy = new Multigraph(this);
            List<int> verticesToIgnore = new List<int>(); // wierzchołki, które po utworzeniu fragmentu cyklu Eulera nie są połączone z resztą grafu
            while(edgesCount > 0)
            {
                bool nonBridgeEdgeExists = false; 
                int next = v;
                for(int i=0;i<degrees.Length; i++)
                {
                    if (copy.multiplicities[v, i] == 0) continue;
                    if (copy.multiplicities[v, i] > 1) // krawedz wielokrotna nie jest mostem
                    {
                        nonBridgeEdgeExists = true;
                        next = i;
                        break;
                    }
                    if(copy.IsBridge(v,i,verticesToIgnore) == false)
                    {
                        nonBridgeEdgeExists = true;
                        next = i;
                        break;
                    }
                }
                if(nonBridgeEdgeExists == true) // wybór 1. krawędzi nie będącej mostem
                {
                    eulerCycle.Add(next);
                    copy.RemoveEdge(v,next);
                    if (copy.degrees[next] == 0) verticesToIgnore.Add(next);
                    if (copy.degrees[v] == 0) verticesToIgnore.Add(v);
                    v = next;
                    --edgesCount;
                }
                else
                {
                    for (int i = 0; i < degrees.Length; i++)
                    {
                        if (copy.multiplicities[v, i] > 0)
                        {
                            eulerCycle.Add(i);
                            copy.RemoveEdge(v, i);
                            if (copy.degrees[i] == 0) verticesToIgnore.Add(i);
                            if (copy.degrees[v] == 0) verticesToIgnore.Add(v);
                            v = i;
                            --edgesCount;                           
                            break;
                        }
                    }
                }
            }

            return eulerCycle;
        }

        public List<int> Christofides(int p)
        {
            List<int> hamilton = new List<int>();
            List<Tuple<int, int>> edges = Mst2Approx.PrimAlgorithm.Execute(this);
            Multigraph mst = new Multigraph(edges.Count + 1);
            foreach (var e in edges)
            {
                mst.SetEdge(e.Item1, e.Item2, distances[e.Item1, e.Item2], 1);
            }
            List<int> odd = new List<int>();
            
            for(int i=0;i<mst.degrees.Length;i++)
            {
                if (mst.degrees[i] % 2 == 1) odd.Add(i);
            }
            List<Tuple<int, int>> matchingEdges = new List<Tuple<int, int>>();

            while(odd.Count > 0) // perfect matching
            {
                double best = double.PositiveInfinity;
                int w = -1;
                int v = odd[0];
                for(int i=0;i<degrees.Length;i++)
                {
                    if (!odd.Contains(i)) continue;
                    if (v == i) continue;
                    if (distances[v, i] < best)
                    {
                        w = i;
                        best = distances[v, i];
                    }
                }
                matchingEdges.Add(new Tuple<int, int>(v, w));
                odd.Remove(v);
                odd.Remove(w);
            }
            if (matchingEdges.Count == 2) p = 1;
            for(int i=0; i<p;i++)
            {
                if (matchingEdges.Count == 1) break;
                Random rand = new Random(Guid.NewGuid().GetHashCode());
                Tuple<int, int> e1 = matchingEdges[rand.Next(matchingEdges.Count)];
                Tuple<int, int> e2 = matchingEdges[rand.Next(matchingEdges.Count)];
                while(e1.Item1 == e2.Item1 && e2.Item2 == e1.Item2) // zeby byly rozne krawedzie
                {
                    e2 = matchingEdges[rand.Next(matchingEdges.Count)];
                }
                double dist = distances[e1.Item1, e1.Item2] + distances[e2.Item1, e2.Item2];

                double candidate1 = distances[e1.Item1, e2.Item1] + distances[e1.Item2, e2.Item2];

                double candidate2 = distances[e1.Item1,e2.Item2] + distances[e1.Item2, e2.Item1];

                if(candidate1 < candidate2)
                {
                    if(candidate1 < dist)
                    {
                        matchingEdges.Add(new Tuple<int, int>(e1.Item1, e2.Item1));
                        matchingEdges.Add(new Tuple<int, int>(e1.Item2,e2.Item2));
                        matchingEdges.Remove(e1);
                        matchingEdges.Remove(e2);
                    }
                }
                else
                {
                    if(candidate2 < dist)
                    {
                        matchingEdges.Add(new Tuple<int, int>(e1.Item1, e2.Item2));
                        matchingEdges.Add(new Tuple<int, int>(e1.Item2, e2.Item1));
                        matchingEdges.Remove(e1);
                        matchingEdges.Remove(e2);
                    }                    
                }
            }
            foreach (var e in matchingEdges)
            {
                int mult = mst.multiplicities[e.Item1, e.Item2];
                mst.SetEdge(e.Item1, e.Item2, distances[e.Item1, e.Item2], mult+1);
            }
            var euler = mst.FindEulerCycle();
            hamilton = EulerToHamilton(euler);
            return hamilton;
        }

        private List<int> EulerToHamilton(List<int> euler) // pierwsze wystąpienia wierzchołków
        {
            List<int> hamilton = new List<int>();
            int largestIndex = -1;
            for (int i = 0; i < euler.Count; i++)
            {
                if (euler[i] > largestIndex) largestIndex = euler[i];
            }
            bool[] visited = new bool[largestIndex + 1];
            for (int i = 0; i < euler.Count; i++)
            {
                if (visited[euler[i]] == false)
                {
                    hamilton.Add(euler[i]);
                    visited[euler[i]] = true;
                }
            }
            return hamilton;
        }
    }
}
