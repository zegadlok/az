﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;
using System.Text.RegularExpressions;

namespace TSPApprox
{
    /// <summary>
    /// Read input graph from text file
    /// </summary>
    public class ReadTSPInstance
    {
        private string _filename { get; set; }

        public ReadTSPInstance(string fileName)
        {
            _filename = fileName;
        }

        public bool Read(ref List<Point> points)
        {
            points = new List<Point>();
            var lines = File.ReadAllLines(_filename);
            for (int i = 0; i < lines.Length; i++)
            {
                var splitted = Regex.Split(lines[i], @"\s+").Where(s => s != string.Empty).ToList();
                if (splitted.Count() != 2)
                {
                    return false;
                }

                double x, y;
                var xOk = double.TryParse(splitted[0], out x);
                var yOk = double.TryParse(splitted[1], out y);
                if (!yOk || !xOk)
                    return false;
                points.Add(new Point(x, y));
            }
            return true;
        }


    }


}
