﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using OxyPlot.Wpf;
using OxyPlot;
using OxyPlot.Series;
using System.IO;
using TSPApprox.Mst2Approx;

namespace TSPApprox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string fileName;
        private List<Point> points;
        private Multigraph graph;
        private List<int> resultOrder;
        private Random random;
        public MainWindow()
        {
            InitializeComponent();
            random = new Random((int)DateTime.Now.Ticks);
            RunAlgorithmsBtn.IsEnabled = false;
            TwoApproxCost.Content = "";
            ChritofidesCost.Content = "";

            //StringBuilder sb = new StringBuilder();
            //for(int v=5;v<=300;v+=5)
            //{
            //    double avgTwo =0;
            //    double avgChr = 0;
            //    for(int i=0;i<10;i++)
            //    {
            //        Multigraph g = new Multigraph(GenerateGraph(0, 0, 10, 10, v));
            //        TSPSolution two = (new TwoApproxAlgorithm()).Execute(g);
            //        var vert = g.Christofides(5);
            //        TSPSolution chr = new TSPSolution(vert, g);
            //        avgTwo += two.Cost;
            //        avgChr += chr.Cost;
            //    }
            //    avgChr /= 10;
            //    avgTwo /= 10;
            //    sb.AppendLine(v.ToString() + ";" + avgTwo.ToString() + ";" + avgChr.ToString());  
            //}
            //File.AppendAllText("wykresy.csv", sb.ToString());
        }

        

        private void OpenFileBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.InitialDirectory = Directory.GetCurrentDirectory();
            dialog.DefaultExt = ".txt";
            dialog.Filter = "Text Files (*.txt)|*.txt";
            var result = dialog.ShowDialog();

            if (result == true)
            {
                fileName = dialog.FileName;
                var read = new ReadTSPInstance(fileName);

                var success = read.Read(ref points);
                if (success)
                {
                    SetInput();
                }
                else
                {
                    MessageBox.Show("Invalid input file");
                }
            }
        }

        private void SetInput()
        {
            RunAlgorithmsBtn.IsEnabled = true;
            ChritofidesCost.Content = "";
            TwoApproxCost.Content = "";
            PlotInputData(TwoApproxPlot);
            PlotInputData(ChristofidesPlot);
            graph = new Multigraph(points);
        }

        private void PlotInputData(PlotView plot)
        {
            plot.Model = new PlotModel()
            {
                IsLegendVisible = false
            };

            var vertexSeries = new OxyPlot.Series.ScatterSeries
            {


                MarkerType = MarkerType.Circle,
                MarkerSize = 8,
                MarkerStroke = OxyColor.FromRgb(198, 54, 49),
                MarkerFill = OxyColor.FromRgb(229, 47, 41),
                MarkerStrokeThickness = 1.0,
                Title = "Vertices",
            };

            for (int i = 0; i < points.Count; i++)
            {
                var point = new ScatterPoint(points[i].X, points[i].Y);
                vertexSeries.Points.Add(point);

                var vertexAnnotation = new OxyPlot.Annotations.PointAnnotation();
                vertexAnnotation.Shape = MarkerType.None;
                vertexAnnotation.X = points[i].X;
                vertexAnnotation.Y = points[i].Y;
                vertexAnnotation.Text = (i + 1).ToString();
                plot.Model.Annotations.Add(vertexAnnotation);
            }

            plot.Model.Series.Add(vertexSeries);
            plot.InvalidatePlot(true);
        }

        private void RunAlgorithmsBtn_Click(object sender, RoutedEventArgs e)
        {
            //Two approx
            var twoApproxAlgorithm = new TwoApproxAlgorithm();
            var twoApproxSolution = twoApproxAlgorithm.Execute(graph);
            AddSolutionSeries(twoApproxSolution, TwoApproxPlot, TwoApproxCost,"2approx");
            // Chritofides
            var vertexList = graph.Christofides(5);
            //vertexList.RemoveAt(vertexList.Count - 1);
            var christofidesSolution = new TSPSolution(vertexList, graph);
            AddSolutionSeries(christofidesSolution, ChristofidesPlot, ChritofidesCost,"christofides");
        }

        private void AddSolutionSeries(TSPSolution sol, PlotView plot, Label costTBox, string algName)
        {
            SaveFileHelper.SaveSolution(sol, fileName,algName);
            costTBox.Content = "Cost: " + sol.Cost;
            var resultSeries = new OxyPlot.Series.LineSeries
            {
                Color = OxyColor.FromRgb(53, 85, 224),
                MarkerType = MarkerType.None,
                Title = "Result",
            };
            for (int i = 0; i < sol.VertexOrder.Count; i++)
            {
                var point = new DataPoint(points[sol.VertexOrder[i]].X, points[sol.VertexOrder[i]].Y);
                resultSeries.Points.Add(point);
            }
            if (sol.VertexOrder.Count > 0)
            {
                var point = new DataPoint(points[sol.VertexOrder[0]].X, points[sol.VertexOrder[0]].Y);
                resultSeries.Points.Add(point);
            }
            if (plot.Model.Series.Any(x => x.Title == "Result"))
            {
                var toDel = plot.Model.Series.First(x => x.Title == "Result");
                plot.Model.Series.Remove(toDel);
            }
            plot.Model.Series.Add(resultSeries);
            plot.InvalidatePlot(true);
        }

        private void GenerateBtn_Click(object sender, RoutedEventArgs e)
        {
            double x1, y1, x2, y2;
            int numOfVertex;
            var x1IsOk = double.TryParse(tBoxX1.Text, out x1);
            var y1IsOk = double.TryParse(tBoxY1.Text, out y1);
            var x2IsOk = double.TryParse(tBoxX2.Text, out x2);
            var y2IsOk = double.TryParse(tBoxY2.Text, out y2);
            var numOfVertexIsOk = int.TryParse(tBoxVertexCount.Text, out numOfVertex) && numOfVertex >= 3;
            if (!x1IsOk || !y1IsOk || !x2IsOk || !y2IsOk || !numOfVertexIsOk)
            {
                MessageBox.Show("Some Input parameter is invalid");
                return;
            }
            var generatedPoints = GenerateGraph(x1, y1, x2, y2, numOfVertex);
            points = generatedPoints;
            fileName = SaveFileHelper.SaveGeneratedGraph(points, Directory.GetCurrentDirectory());
            SetInput();
        }

        private List<Point> GenerateGraph(double x1, double y1, double x2, double y2, int numOfVertices)
        {
            double xMin, xMax, yMin, yMax, xDiff, yDiff;
            if (x1 > x2)
            {
                xMax = x1;
                xMin = x2;
            }
            else
            {
                xMax = x2;
                xMin = x1;
            }

            if (y1 > y2)
            {
                yMax = y1;
                yMin = y2;
            }
            else
            {
                yMax = y2;
                yMin = y1;
            }
            xDiff = xMax - xMin;
            yDiff = yMax - yMin;

            var generatedPoints = new List<Point>();

            for (int i = 0; i < numOfVertices; i++)
            {
                var randomX = random.NextDouble();
                var randomY = random.NextDouble();
                var xShifted = xMin + randomX * xDiff;
                var yShifted = yMin + randomY * yDiff;
                generatedPoints.Add(new Point(xShifted, yShifted));
            }
            return generatedPoints;
        }
    }
}
