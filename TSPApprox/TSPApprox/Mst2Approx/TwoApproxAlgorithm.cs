﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSPApprox.Mst2Approx
{
    public class TwoApproxAlgorithm
    {

        public TSPSolution Execute(Multigraph g)
        {
            List<Tuple<int, int>> edges = Mst2Approx.PrimAlgorithm.Execute(g);
            Multigraph mst = new Multigraph(edges.Count + 1);

            foreach (var e in edges)
            {
                mst.SetEdge(e.Item1, e.Item2, g.distances[e.Item1, e.Item2], 1);
            }
            var hamilton = PreorderWalk(mst);


            return new TSPSolution(hamilton, g);


        }

        private List<int> PreorderWalk(Multigraph mst)
        {
            List<int> hamilton = new List<int>();
            bool[] visited = new bool[mst.VerticesCount];
            Stack<int> s = new Stack<int>();
            s.Push(0);
            while (s.Count > 0)
            {
                int v = s.Pop();
                visited[v] = true;
                hamilton.Add(v);
                for (int i = 0; i < mst.VerticesCount; i++)
                {
                    if (visited[i] == false && mst.multiplicities[v, i] > 0) s.Push(i);
                }
            }

            return hamilton;
        }

    }
}
