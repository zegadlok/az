﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSPApprox.Mst2Approx
{
    /// <summary>
    /// priority queue based on heap for Prim's algorithm
    /// </summary>
    public class PriorityQueue
    {
        private QueueItem[] Tab;
        private VertexHeapInfo[] Info;

        private int Size;
        public QueueItem this[int i] { get { return Tab[i]; } }

        public bool IsEmpty()  { return Size == 0;}

        public PriorityQueue(IList<QueueItem> items, int verticesCount)
        {
            Tab = new QueueItem[items.Count + 1];
            Tab[0] = new QueueItem //wartownik
            {
                DistanceToTree = double.MinValue
            };

            for (int i = 1; i <= items.Count; i++)
                Tab[i] = items[i - 1];

            CreateHeap(verticesCount);
        }

        public QueueItem RemoveMinimal(out VertexHeapInfo[] heapInfo)
        {
            var item = Tab[1];
            Info[item.Vertex].IsInQueue = false;
            Tab[1] = Tab[Size];
            Size--;
            DownHeap(1);
            heapInfo = Info;
            return item;
        }

        public void DecreaseValue(int ind, double distanceToTree, int nearestVertex, out VertexHeapInfo[] heapInfo)
        {
            Tab[ind].DistanceToTree = distanceToTree;
            Tab[ind].NearestVertex = nearestVertex;
            UpHeap(ind);
            heapInfo = Info;
        }

        private void CreateHeap(int verticesCount)
        {
            Size = Tab.Length - 1;

            Info = new VertexHeapInfo[verticesCount];
            for (int i = 0; i < Info.Length; i++)
                Info[i] = new VertexHeapInfo();

            for (int i = Size / 2; i >= 1; i--)
                DownHeap(i);

            for (int i = 1; i <= Size; i++)
            {
                var elem = Tab[i];
                Info[elem.Vertex].IndexInHeap = i;
                Info[elem.Vertex].IsInQueue = true;
            }
        }

        private void UpHeap(int ind)
        {
            var elem = Tab[ind];
            while (Tab[ind / 2].DistanceToTree > elem.DistanceToTree)
            {
                Tab[ind] = Tab[ind / 2];
                Info[Tab[ind].Vertex].IndexInHeap = ind;
                ind /= 2;
            }
            Tab[ind] = elem;
            Info[Tab[ind].Vertex].IndexInHeap = ind;
        }

        private void DownHeap(int ind)
        {
            var elem = Tab[ind];
            var k = 2 * ind;

            while (k <= Size)
            {
                if (k + 1 <= Size)
                {
                    if (Tab[k + 1].DistanceToTree < Tab[k].DistanceToTree)
                    {
                        k++;
                    }
                }
                if (Tab[k].DistanceToTree < elem.DistanceToTree)
                {
                    Tab[ind] = Tab[k];
                    Info[Tab[ind].Vertex].IndexInHeap = ind;
                    ind = k;
                    k = 2 * ind;
                }
                else
                {
                    break;
                }
            }
            Tab[ind] = elem;
            Info[Tab[ind].Vertex].IndexInHeap = ind;
        }
    }

    public class QueueItem
    {
        public int Vertex { get; set; }
        public double DistanceToTree { get; set; }
        public int NearestVertex { get; set; }
    }

    /// <summary>
    /// In order to assure fast search of elements in  heap
    /// </summary>
    public class VertexHeapInfo
    {
        public int IndexInHeap { get; set; }
        public bool IsInQueue { get; set; }
    }
}
