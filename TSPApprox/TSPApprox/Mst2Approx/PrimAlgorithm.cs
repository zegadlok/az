﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSPApprox.Mst2Approx
{
    /// <summary>
    /// Prim's algorithm for minimum spanning tree 
    /// </summary>
    public static class PrimAlgorithm
    {
        public static List<Tuple<int, int>> Execute(Multigraph g)
        {
            var result = new List<Tuple<int, int>>();

            var queue = Initialize(g);
            while (!queue.IsEmpty())
            {
                 VertexHeapInfo[] heapInfos;
                var minimal = queue.RemoveMinimal(out heapInfos);
                result.Add(new Tuple<int, int>(minimal.Vertex, minimal.NearestVertex));
                for (int i = 0; i < g.VerticesCount; i++)
                {
                    if (heapInfos[i].IsInQueue && g.distances[minimal.Vertex, i] < queue[heapInfos[i].IndexInHeap].DistanceToTree)
                    {
                        queue.DecreaseValue(heapInfos[i].IndexInHeap, g.distances[minimal.Vertex, i], minimal.Vertex, out heapInfos);
                    }
                }
            }
            return result;
        }

        private static PriorityQueue Initialize(Multigraph g)
        {
            //choose vertex 0 as root of MST
            var elems = new List<QueueItem>();

            for (int i = 1; i < g.VerticesCount; i++)
            {
                elems.Add(
                    new QueueItem
                    {
                        Vertex = i,
                        NearestVertex = 0,
                        DistanceToTree = g.distances[0, i]
                    }
                    );
            }
            var queue = new PriorityQueue(elems, g.VerticesCount);
            return queue;
        }
    }


}
