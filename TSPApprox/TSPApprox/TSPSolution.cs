﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSPApprox
{
    public class TSPSolution
    {
        public List<int> VertexOrder { get; set; }
        public double Cost { get; set; }

        public TSPSolution(List<int> vertexOrder, Multigraph graph)
        {

            VertexOrder = vertexOrder;
            Cost = 0;

            for (int i = 0; i < vertexOrder.Count; i++)
            {
                Cost += graph.distances[vertexOrder[i], vertexOrder[(i + 1) % vertexOrder.Count]];
            }
        }
    }

}
