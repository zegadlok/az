﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace TSPApprox
{
    public static class SaveFileHelper
    {

        public static string SaveGeneratedGraph(List<Point> points, string path)
        {
            var time = String.Format("{0:s}", DateTime.Now).Replace(":", "_");
            var fileName = path + @"\GeneratedInstance_" + time + ".txt";
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(fileName))
            {
                for (int i = 0; i < points.Count; i++)
                {
                    var line = points[i].X.ToString() + " " + points[i].Y.ToString();
                    file.WriteLine(line);
                }
            }
            return fileName;
        }

        public static void SaveSolution(TSPSolution sol, string path, string algName)
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(path.Substring(0, path.Length - 4) + "_" + algName + @"_solution.txt"))
            {

                var firstLine = sol.Cost.ToString();
                var secondLine = String.Join(" ", sol.VertexOrder.Select(x => x + 1));
                file.WriteLine(firstLine);
                file.WriteLine(secondLine);
            }
        }
    }

}
